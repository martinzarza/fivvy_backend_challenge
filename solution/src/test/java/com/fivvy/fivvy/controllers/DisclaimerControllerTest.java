package com.fivvy.fivvy.controllers;

import com.fivvy.fivvy.AbstractTest;
import com.fivvy.fivvy.dtos.disclaimer.DisclaimerRequestDTO;
import com.fivvy.fivvy.models.Disclaimer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DisclaimerControllerTest extends AbstractTest {
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    private DisclaimerRequestDTO createDisclaimerRequestDTO() {
        return new DisclaimerRequestDTO(this.randomString(), this.randomString(), this.randomString());
    }

    @Test
    public void Test_WHEN_DisclaimerCreated_EXPECT_ValidationError() throws Exception {
        DisclaimerRequestDTO newDisclaimer = this.createDisclaimerRequestDTO();
        newDisclaimer.setName(null);

        // Create Disclaimer
        MvcResult response = this.post("/disclaimers", newDisclaimer);

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), status);
    }

    @Test
    public void Test_WHEN_DisclaimerCreated_EXPECT_SuccessfulCreation() throws Exception {
        DisclaimerRequestDTO newDisclaimer = this.createDisclaimerRequestDTO();

        // Create Disclaimer
        MvcResult response = this.post("/disclaimers", newDisclaimer);

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), status);

        Disclaimer createdDisclaimer = this.mapFromJson(response.getResponse().getContentAsString(), Disclaimer.class);
        assertNotNull(createdDisclaimer.getId());

        // Get Disclaimer and check values
        response = this.get("/disclaimers/" + createdDisclaimer.getId());

        Disclaimer disclaimer = this.mapFromJson(response.getResponse().getContentAsString(), Disclaimer.class);
        assertEquals(newDisclaimer.getName(), disclaimer.getName());
        assertEquals(newDisclaimer.getText(), disclaimer.getText());
        assertEquals(newDisclaimer.getVersion(), disclaimer.getVersion());
    }

    @Test
    public void Test_WHEN_DisclaimerUpdated_EXPECT_NotFoundError() throws Exception {
        DisclaimerRequestDTO newDisclaimer = this.createDisclaimerRequestDTO();

        // Update disclaimer
        MvcResult response = this.put("/disclaimers/" + UUID.randomUUID(), newDisclaimer);

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.NOT_FOUND.value(), status);
    }

    @Test
    public void Test_WHEN_DisclaimerUpdated_EXPECT_SuccessfulUpdate() throws Exception {
        DisclaimerRequestDTO newDisclaimer = this.createDisclaimerRequestDTO();

        // Create Disclaimer
        MvcResult response = this.post("/disclaimers", newDisclaimer);

        Disclaimer createdDisclaimer = this.mapFromJson(response.getResponse().getContentAsString(), Disclaimer.class);
        assertNotNull(createdDisclaimer.getId());

        // Update disclaimer
        newDisclaimer = this.createDisclaimerRequestDTO();
        response = this.put("/disclaimers/" + createdDisclaimer.getId(), newDisclaimer);

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.OK.value(), status);

        // Get Disclaimer and check values
        response = this.get("/disclaimers/" + createdDisclaimer.getId());

        Disclaimer disclaimer = this.mapFromJson(response.getResponse().getContentAsString(), Disclaimer.class);
        assertEquals(newDisclaimer.getName(), disclaimer.getName());
        assertEquals(newDisclaimer.getText(), disclaimer.getText());
        assertEquals(newDisclaimer.getVersion(), disclaimer.getVersion());
    }

    @Test
    public void Test_WHEN_DisclaimerDeleted_EXPECT_NotFoundError() throws Exception {
        // Delete disclaimer
        MvcResult response = this.delete("/disclaimers/" + UUID.randomUUID());

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.NOT_FOUND.value(), status);
    }

    @Test
    public void Test_WHEN_DisclaimerDeleted_EXPECT_SuccessfulDeletion() throws Exception {
        DisclaimerRequestDTO newDisclaimer = this.createDisclaimerRequestDTO();

        // Create Disclaimer
        MvcResult response = this.post("/disclaimers", newDisclaimer);

        Disclaimer createdDisclaimer = this.mapFromJson(response.getResponse().getContentAsString(), Disclaimer.class);
        assertNotNull(createdDisclaimer.getId());

        // Delete disclaimer
        response = this.delete("/disclaimers/" + createdDisclaimer.getId());

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.NO_CONTENT.value(), status);

        // Get Disclaimer and check values
        response = this.get("/disclaimers/" + createdDisclaimer.getId());

        status = response.getResponse().getStatus();
        assertEquals(HttpStatus.NOT_FOUND.value(), status);
    }

    @Test
    public void Test_WHEN_DisclaimersAreFetched_EXPECT_SuccessfulFetch() throws Exception {
        MvcResult response = this.get("/disclaimers");

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.OK.value(), status);

        List<Disclaimer> disclaimers = Arrays.asList(this.mapFromJson(response.getResponse().getContentAsString(),
                Disclaimer[].class));

        assertTrue(disclaimers.size() > 0);
    }
}
