package com.fivvy.fivvy.controllers;

import com.fivvy.fivvy.AbstractTest;
import com.fivvy.fivvy.dtos.acceptance.AcceptanceRequestDTO;
import com.fivvy.fivvy.dtos.disclaimer.DisclaimerRequestDTO;
import com.fivvy.fivvy.models.Acceptance;
import com.fivvy.fivvy.models.Disclaimer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AcceptanceControllerTest extends AbstractTest {
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    private DisclaimerRequestDTO createDisclaimerRequestDTO() {
        return new DisclaimerRequestDTO(this.randomString(), this.randomString(), this.randomString());
    }

    private AcceptanceRequestDTO createAcceptanceRequestDTO(String disclaimerId) {
        return new AcceptanceRequestDTO(disclaimerId, this.randomString());
    }

    @Test
    public void Test_WHEN_AcceptanceCreated_EXPECT_ValidationError() throws Exception {
        // Create Disclaimer
        DisclaimerRequestDTO newDisclaimer = this.createDisclaimerRequestDTO();

        MvcResult response = this.post("/disclaimers", newDisclaimer);
        Disclaimer createdDisclaimer = this.mapFromJson(response.getResponse().getContentAsString(), Disclaimer.class);

        // Create Acceptance
        AcceptanceRequestDTO newAcceptance = this.createAcceptanceRequestDTO(createdDisclaimer.getId());
        newAcceptance.setUserId(null);

        response = this.post("/acceptances", newAcceptance);

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.BAD_REQUEST.value(), status);
    }

    @Test
    public void Test_WHEN_AcceptanceCreated_EXPECT_SuccessfulCreation() throws Exception {
        // Create Disclaimer
        DisclaimerRequestDTO newDisclaimer = this.createDisclaimerRequestDTO();

        MvcResult response = this.post("/disclaimers", newDisclaimer);
        Disclaimer createdDisclaimer = this.mapFromJson(response.getResponse().getContentAsString(), Disclaimer.class);

        // Create Acceptance
        AcceptanceRequestDTO newAcceptance = this.createAcceptanceRequestDTO(createdDisclaimer.getId());

        response = this.post("/acceptances", newAcceptance);

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.CREATED.value(), status);
    }

    @Test
    public void Test_WHEN_AcceptancesAreFetched_EXPECT_SuccessfulFetch() throws Exception {
        MvcResult response = this.get("/acceptances");

        int status = response.getResponse().getStatus();
        assertEquals(HttpStatus.OK.value(), status);

        List<Acceptance> acceptances = Arrays.asList(this.mapFromJson(response.getResponse().getContentAsString(),
                Acceptance[].class));

        assertTrue(acceptances.size() > 0);
    }
}
