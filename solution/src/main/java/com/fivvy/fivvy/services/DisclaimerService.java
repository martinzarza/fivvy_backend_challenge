package com.fivvy.fivvy.services;

import com.fivvy.fivvy.models.Acceptance;
import com.fivvy.fivvy.models.Disclaimer;
import com.fivvy.fivvy.repositories.DisclaimerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DisclaimerService {
    private final AcceptanceService acceptanceService;

    private final DisclaimerRepository disclaimerRepository;

    @Autowired
    public DisclaimerService(AcceptanceService acceptanceService, DisclaimerRepository disclaimerRepository) {
        this.acceptanceService = acceptanceService;
        this.disclaimerRepository = disclaimerRepository;
    }

    public Disclaimer create(Disclaimer disclaimer) {
        disclaimer.setCreatedAt(LocalDateTime.now());
        return disclaimerRepository.save(disclaimer);
    }

    public List<Disclaimer> getAll(String text) {
        if (text != null) {
            return disclaimerRepository.findAllByTextContainingIgnoreCase(text);
        }
        return (List<Disclaimer>) disclaimerRepository.findAll();
    }

    public Disclaimer get(String disclaimerId) {
        return disclaimerRepository.findById(disclaimerId)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "Disclaimer with ID " + disclaimerId + " not found."
                ));
    }

    public Disclaimer update(String disclaimerId, Disclaimer newDisclaimer) {
        Disclaimer disclaimer = this.get(disclaimerId);

        disclaimer.setName(newDisclaimer.getName());
        disclaimer.setText(newDisclaimer.getText());
        disclaimer.setVersion(newDisclaimer.getVersion());
        disclaimer.setUpdatedAt(LocalDateTime.now());

        return disclaimerRepository.save(disclaimer);
    }

    public void delete(String disclaimerId) {
        Disclaimer disclaimer = this.get(disclaimerId);

        List<Acceptance> acceptances = acceptanceService.getAll(disclaimer.getId(), null);
        if (acceptances.size() > 0) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Disclaimer with ID " + disclaimerId + " cannot be deleted as it has " +
                    "user acceptances linked to it."
            );
        }

        disclaimerRepository.deleteById(disclaimerId);
    }
}
