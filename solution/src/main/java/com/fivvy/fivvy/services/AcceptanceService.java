package com.fivvy.fivvy.services;

import com.fivvy.fivvy.models.Acceptance;
import com.fivvy.fivvy.repositories.AcceptanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AcceptanceService {
    private final AcceptanceRepository acceptanceRepository;

    @Autowired
    public AcceptanceService(AcceptanceRepository acceptanceRepository) {
        this.acceptanceRepository = acceptanceRepository;
    }

    public Acceptance create(Acceptance acceptance) {
        acceptance.setCreatedAt(LocalDateTime.now());
        return acceptanceRepository.save(acceptance);
    }

    public List<Acceptance> getAll(String disclaimerId, String userId) {
        if (disclaimerId != null && userId != null) {
            return acceptanceRepository.findAllByDisclaimerIdAndUserId(disclaimerId, userId);

        } else if (disclaimerId != null) {
            return acceptanceRepository.findAllByDisclaimerId(disclaimerId);

        } else if (userId != null) {
            return acceptanceRepository.findAllByUserId(userId);
        }

        return (List<Acceptance>) acceptanceRepository.findAll();
    }
}
