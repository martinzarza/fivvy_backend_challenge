package com.fivvy.fivvy.repositories;

import com.fivvy.fivvy.models.Acceptance;
import com.fivvy.fivvy.models.AcceptanceId;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface AcceptanceRepository extends CrudRepository<Acceptance, AcceptanceId> {
    List<Acceptance> findAllByDisclaimerId(String disclaimerId);

    List<Acceptance> findAllByUserId(String userId);

    List<Acceptance> findAllByDisclaimerIdAndUserId(String disclaimerId, String userId);
}
