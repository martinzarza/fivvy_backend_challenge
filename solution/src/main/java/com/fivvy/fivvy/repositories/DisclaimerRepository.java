package com.fivvy.fivvy.repositories;

import com.fivvy.fivvy.models.Disclaimer;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface DisclaimerRepository extends CrudRepository<Disclaimer, String> {
    List<Disclaimer> findAllByTextContainingIgnoreCase(String text);
}
