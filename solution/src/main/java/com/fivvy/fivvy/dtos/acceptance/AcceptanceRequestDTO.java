package com.fivvy.fivvy.dtos.acceptance;

import com.fivvy.fivvy.models.Acceptance;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AcceptanceRequestDTO {
    @NotBlank(message = "DisclaimerId is mandatory")
    private String disclaimerId;

    @NotBlank(message = "UserId is mandatory")
    private String userId;

    public Acceptance convertToEntity(ModelMapper modelMapper) {
        return modelMapper.map(this, Acceptance.class);
    }
}
