package com.fivvy.fivvy.dtos.disclaimer;

import com.fivvy.fivvy.models.Disclaimer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DisclaimerRequestDTO {
    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotBlank(message = "Text is mandatory")
    private String text;

    @NotBlank(message = "Version is mandatory")
    private String version;

    public Disclaimer convertToEntity(ModelMapper modelMapper) {
        return modelMapper.map(this, Disclaimer.class);
    }
}
