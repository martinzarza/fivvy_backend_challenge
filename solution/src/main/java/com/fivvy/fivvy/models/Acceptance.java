package com.fivvy.fivvy.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.fivvy.fivvy.models.converters.LocalDateTimeConverter;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@DynamoDBTable(tableName = "Acceptance")
public class Acceptance {

    @Id
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private AcceptanceId acceptanceId;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    private LocalDateTime createdAt;

    @DynamoDBHashKey
    public String getDisclaimerId() {
        return acceptanceId != null ? acceptanceId.getDisclaimerId() : null;
    }

    public void setDisclaimerId(String disclaimerId) {
        if (acceptanceId == null) {
            acceptanceId = new AcceptanceId();
        }
        acceptanceId.setDisclaimerId(disclaimerId);
    }

    @DynamoDBRangeKey
    public String getUserId() {
        return acceptanceId != null ? acceptanceId.getUserId() : null;
    }

    public void setUserId(String userId) {
        if (acceptanceId == null) {
            acceptanceId = new AcceptanceId();
        }
        acceptanceId.setUserId(userId);
    }

}
