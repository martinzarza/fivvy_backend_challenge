package com.fivvy.fivvy.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class AcceptanceId implements Serializable {

    @DynamoDBHashKey
    private String disclaimerId;

    @DynamoDBRangeKey
    private String userId;

}
