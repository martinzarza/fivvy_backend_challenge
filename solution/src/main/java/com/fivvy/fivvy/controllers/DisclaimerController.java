package com.fivvy.fivvy.controllers;

import com.fivvy.fivvy.dtos.disclaimer.DisclaimerRequestDTO;
import com.fivvy.fivvy.models.Disclaimer;
import com.fivvy.fivvy.services.DisclaimerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/disclaimers")
public class DisclaimerController {
    private final DisclaimerService disclaimerService;
    private final ModelMapper modelMapper;

    @Autowired
    public DisclaimerController(DisclaimerService disclaimerService, ModelMapper modelMapper) {
        this.disclaimerService = disclaimerService;
        this.modelMapper = modelMapper;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Disclaimer create(
            @RequestBody @Valid DisclaimerRequestDTO disclaimerRequest
    ) {
        return disclaimerService.create(disclaimerRequest.convertToEntity(modelMapper));
    }

    @GetMapping
    public List<Disclaimer> getAll(
            @RequestParam(name = "text", required = false) String text
    ) {
        return disclaimerService.getAll(text);
    }

    @GetMapping("/{disclaimerId}")
    public Disclaimer get(
            @PathVariable("disclaimerId") String disclaimerId
    ) {
        return disclaimerService.get(disclaimerId);
    }

    @PutMapping("/{disclaimerId}")
    public Disclaimer update(
            @PathVariable("disclaimerId") String disclaimerId,
            @RequestBody @Valid DisclaimerRequestDTO disclaimerRequest
    ) {
        return disclaimerService.update(disclaimerId, disclaimerRequest.convertToEntity(modelMapper));
    }

    @DeleteMapping("/{disclaimerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(
            @PathVariable("disclaimerId") String disclaimerId
    ) {
        disclaimerService.delete(disclaimerId);
    }
}
