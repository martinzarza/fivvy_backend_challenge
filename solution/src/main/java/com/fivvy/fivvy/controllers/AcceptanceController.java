package com.fivvy.fivvy.controllers;

import com.fivvy.fivvy.dtos.acceptance.AcceptanceRequestDTO;
import com.fivvy.fivvy.models.Acceptance;
import com.fivvy.fivvy.services.AcceptanceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/acceptances")
public class AcceptanceController {
    private final AcceptanceService acceptanceService;
    private final ModelMapper modelMapper;

    @Autowired
    public AcceptanceController(AcceptanceService acceptanceService, ModelMapper modelMapper) {
        this.acceptanceService = acceptanceService;
        this.modelMapper = modelMapper;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acceptance create(
            @RequestBody @Valid AcceptanceRequestDTO acceptanceRequest
    ) {
        return acceptanceService.create(acceptanceRequest.convertToEntity(modelMapper));
    }

    @GetMapping
    public List<Acceptance> getAll(
            @RequestParam(name = "user_id", required = false) String userId
    ) {
        return acceptanceService.getAll(null, userId);
    }
}
