
# Fivvy Backend Challenge

Simple API to manage disclaimer acceptance of terms and conditions.

## Usage
To run this app locally, it is necessary to have [Docker](https://www.docker.com/products/docker-desktop/) and Docker Compose installed.

After the installation, run the following command:

```docker-compose -f ./solution/docker-compose.yaml -p fivvy-application up --build -d```

This will build and start the Spring Boot application and the DynamoDB instance used by the app. If everything went well, the API will now be listening to new requests on ```http://localhost:8080```.

Download [here](solution/postman.json) the Postman collection to start making requests to it.

## Tests
To execute the tests integrated into the app, it is necessary to have [Maven](https://maven.apache.org/download.cgi) installed.

Then run the following command:

```mvn -f ./solution/pom.xml clean test-compile test```

## API Documentation
## Disclaimers
#### Create Disclaimer
**URL**: POST /disclaimers \
**Request Body**
```json
{
	"name": "Disclaimer",
	"text": "This is an example of a disclaimer text.",
	"version": "1.0.0"
}
```
**Response Status**: 201 - Created \
**Response Body**
```json
{
	"id": "ddc1b192-2e22-448e-a812-731354cd892e",
	"name": "Disclaimer",
	"text": "This is an example of a disclaimer text.",
	"version": "1.0.0",
	"createdAt": "2023-10-02T18:48:05",
	"updatedAt": null 
}
```
***
#### Update Disclaimer
**URL**: PUT /disclaimers/{disclaimerId} \
**Request Body**
```json
{
	"name": "Disclaimer",
	"text": "This is an example of a disclaimer text.",
	"version": "1.0.0"
}
```
**Response Status**: 200 - OK \
**Response Body**
```json
{
	"id": "ddc1b192-2e22-448e-a812-731354cd892e",
	"name": "Disclaimer",
	"text": "This is an example of a disclaimer text.",
	"version": "1.0.0",
	"createdAt": "2023-10-02T18:48:05",
	"updatedAt": "2023-10-02T18:49:05" 
}
```
***
#### Get All Disclaimers
**URL**: GET /disclaimers \
**Request Params**

- text: Optional[string]

**Response Status**: 200 - OK \
**Response Body**
```json
[
	{
		"id": "ddc1b192-2e22-448e-a812-731354cd892e",
		"name": "Disclaimer",
		"text": "This is an example of a disclaimer text.",
		"version": "1.0.0",
		"createdAt": "2023-10-02T18:48:05",
		"updatedAt": "2023-10-02T18:49:05" 
	}
]
```
***
#### Get Disclaimer By Id
**URL**: GET /disclaimers/{disclaimerId} \
**Response Status**: 200 - OK \
**Response Body**
```json
{
	"id": "ddc1b192-2e22-448e-a812-731354cd892e",
	"name": "Disclaimer",
	"text": "This is an example of a disclaimer text.",
	"version": "1.0.0",
	"createdAt": "2023-10-02T18:48:05",
	"updatedAt": "2023-10-02T18:49:05" 
}
```
***
#### Delete Disclaimer
**URL**: DELETE /disclaimers/{disclaimerId} \
**Response Status**: 204 - No Content
***

## Acceptances
#### Create Acceptance
**URL**: POST /acceptances \
**Request Body**
```json
{
	"disclaimerId": "bee99518-e1cb-4c22-bb2e-c7842160a8e6",
	"userId": "04177ffe-1883-4a01-b92d-3bb8120192db"
}
```
**Response Status**: 201 - Created \
**Response Body**
```json
{
	"disclaimerId": "bee99518-e1cb-4c22-bb2e-c7842160a8e6",
	"userId": "04177ffe-1883-4a01-b92d-3bb8120192db",
	"createdAt": "2023-10-02T18:48:05"
}
```
***
#### Get All Acceptances
**URL**: GET /acceptances \
**Request Params**

- user_id: Optional[string]

**Response Status**: 200 - OK \
**Response Body**
```json
[
	{
		"disclaimerId": "bee99518-e1cb-4c22-bb2e-c7842160a8e6",
		"userId": "04177ffe-1883-4a01-b92d-3bb8120192db",
		"createdAt": "2023-10-02T18:48:05"
	}
]
```